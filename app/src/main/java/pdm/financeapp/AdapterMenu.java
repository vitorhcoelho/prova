package pdm.financeapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterMenu extends RecyclerView.Adapter<AdapterMenu.mViewHolder> {

    public static List<Registro> listRegistros;
    public static Registro registro;

    //=======================================================================================================================================
    public AdapterMenu() {
    }

    //=======================================================================================================================================
    public AdapterMenu(List<Registro> listRegistros) {
        this.listRegistros = listRegistros;
    }

    //=======================================================================================================================================
    @NonNull
    @Override
    public mViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sample_menu, parent, false);
        return new mViewHolder(view);
    }

    //=======================================================================================================================================
    @Override
    public void onBindViewHolder(@NonNull mViewHolder holder, int position) {
        registro = listRegistros.get(position);

        holder.registerName.setText(registro.getName() + " - ");
        holder.registerPrice.setText("R$ " + Double.toString(registro.getPrice()));
        holder.registerDescription.setText(registro.getDescription());
    }

    //=======================================================================================================================================
    @Override
    public int getItemCount() {
        return listRegistros.size();
    }

    //=======================================================================================================================================

    /**
     * InnerClass para definir o viewHolder da parentClass
     */
    public class mViewHolder extends RecyclerView.ViewHolder {

        TextView registerName;
        TextView registerPrice;
        TextView registerDescription;

        //=======================================================================================================================================
        public mViewHolder(@NonNull View itemView) {
            super(itemView);

            registerName = itemView.findViewById(R.id.registerName);
            registerPrice = itemView.findViewById(R.id.registerPrice);
            registerDescription = itemView.findViewById(R.id.registerDescription);
        }
    }

}
