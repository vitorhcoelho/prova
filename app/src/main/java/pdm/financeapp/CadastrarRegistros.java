package pdm.financeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.google.android.material.textfield.TextInputLayout;


public class CadastrarRegistros extends AppCompatActivity {

    private EditText registerName;
    private EditText registerPrice;
    private EditText registerDescription;
    private static final String DATA_REQUEST = "data_request";
    ViewGroup parent;

    //==============================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_registros);

        registerName = findViewById(R.id.editTextRegisterName);
        registerPrice = findViewById(R.id.editTextPrice);
        registerDescription = findViewById(R.id.editTextDescription);
        parent = findViewById(R.id.registerRegister);
    }

    //==============================================================================================
    public void saveRegister(View view) {
        if (!validForm(parent)) {
            Registro registro = new Registro(registerName.getText().toString(), Double.parseDouble(registerPrice.getText().toString()),
                    registerDescription.getText().toString());
            Log.d("CADASTRAR_REGISTRO", "" + registro);
            Intent intent = new Intent(this, VisualizarRegistros.class);
            intent.putExtra(DATA_REQUEST, registro);
            startActivity(intent);
        }
    }

    //==============================================================================================
    private boolean validForm(ViewGroup parent) {
        boolean isInvalidContent = false;
        EditText editText;

        try {
            for (int i = 0; i < parent.getChildCount(); i++) {
                View currentChild = parent.getChildAt(i);
                if (currentChild instanceof TextInputLayout) {
                    View frameLayout = ((ViewGroup) currentChild).getChildAt(0);
                    if (frameLayout instanceof FrameLayout) {
                        editText = (EditText) ((FrameLayout) frameLayout).getChildAt(0);
                        if (editText.getText().toString().isEmpty()) {
                            isInvalidContent = true;
                            String error = editText.getTag().toString();
                            ((TextInputLayout) currentChild).setError(error);
                            Log.d("VALID_FORM", "Empty Field -> " + error);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.w("VALID_FORM", "-> " + e.getMessage());
        }

        Log.d("VALID_FORM", "-> isInvalidContent: " + isInvalidContent);
        return isInvalidContent;
    }
}
