package pdm.financeapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class VisualizarRegistros extends AppCompatActivity {

    private AdapterMenu adapterMenu;
    private RecyclerView recyclerView;
    private static List<Registro> listRegistro;
    private static final String DATA_REQUEST = "data_request";
    private static final String ORIGIN = "origin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_registros);

        if(listRegistro == null){
            listRegistro = new ArrayList<>();
        }

        int origin = getIntent().getIntExtra(ORIGIN, 2);
        if(origin == 2){
            Registro registro = (Registro)getIntent().getParcelableExtra(DATA_REQUEST);
            Log.d("MENU_REGISTER_REQUEST", "" + registro);
            listRegistro.add(registro);
        }

        adapterMenu = new AdapterMenu(listRegistro);

        //Config recyclerView
        recyclerView = findViewById(R.id.recyclerMenu);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        recyclerView.setAdapter(adapterMenu);

    }
}
