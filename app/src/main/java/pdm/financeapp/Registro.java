package pdm.financeapp;

import android.os.Parcel;
import android.os.Parcelable;

public class Registro implements Parcelable {

    private String name;
    private Double price;
    private String description;

    //==================================================================================================
    public Registro() {
    }

    //==================================================================================================

    public Registro(String name, Double price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    //==================================================================================================

    public Registro(Parcel in) {
        name = in.readString();

        price = in.readDouble();

        description = in.readString();
    }

    //==================================================================================================
    public static final Creator<Registro> CREATOR = new Creator<Registro>() {
        @Override
        public Registro createFromParcel(Parcel in) {
            return new Registro(in);
        }

        //==================================================================================================
        @Override
        public Registro[] newArray(int size) {
            return new Registro[size];
        }
    };

    //==================================================================================================
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //==================================================================================================
    @Override
    public int describeContents() {
        return hashCode();
    }

    //==================================================================================================
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(price);
        dest.writeString(description);
    }

    //==================================================================================================
    @Override
    public String toString() {
        return "Registro{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
